<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id', 'product_image', 'product_price', 'product_qty', 'customer_name', 'email', 'mobile', 'address'
    ];
}
