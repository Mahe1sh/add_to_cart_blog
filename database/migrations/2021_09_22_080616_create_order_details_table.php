<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->integer("product_id")->nullable();
            $table->string("product_image", 255)->nullable();
            $table->decimal("product_price", 6, 2);
            $table->string("product_qty", 255)->nullable();
            $table->string("customer_name", 255)->nullable();
            $table->string("email", 255)->nullable();
            $table->string("mobile", 255)->nullable();
            $table->string("address", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
