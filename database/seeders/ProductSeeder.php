<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name' => 'Men Slim Fit Checkered Casual Shirt',
                'short_description' => 'Fits:Slim|Size:XL',
                'image' => 'https://rukminim1.flixcart.com/image/880/1056/kc0u7bk0/shirt/5/l/g/l-pk19sh09n-surhi-original-imaft8ttegs2zxzh.jpeg?q=50',
                'price' => 100,
                'status' => 'active'
            ],
            [
                'name' => 'Men Slim Fit Solid Slim Collar Casual Shirt',
                'short_description' => 'Fits:Slim|Size:S',
                'image' => 'https://rukminim1.flixcart.com/image/880/1056/kgwld3k0-0/shirt/e/d/b/m-1165-1168-trendfull-original-imafxfcz2gbvsgfx.jpeg?q=50',
                'price' => 500,
                'status' => 'active'
            ],
            [
                'name' => 'Men Regular Fit Solid Pure Black Casual Shirt',
                'short_description' => 'Fits:Regular|Size:L',
                'image' => 'https://rukminim1.flixcart.com/image/880/1056/kfoapow0-0/shirt/1/l/t/m-c1276-gespo-original-imafw2gqdztqdkyu.jpeg?q=50',
                'price' => 400,
                'status' => 'active'
            ],
            [
                'name' => 'Men Slim Fit Checkered Casual Shirt',
                'short_description' => 'Fits:Slim|Size:39',
                'image' => 'https://rukminim1.flixcart.com/image/880/1056/kmp7ngw0/shirt/g/t/f/m-2000202202-peter-field-original-imagfjzgegs3heg5.jpeg?q=50',
                'price' => 200,
                'status' => 'active'
            ]
        ];
  
        foreach ($products as $key => $value) {
            Product::create($value);
        }
    }
}
